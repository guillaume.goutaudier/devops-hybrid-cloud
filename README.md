# Multi-Cloud DevOps Workshop
The objective of this workshop is to demonstrate how you can build a multi-cloud CI/CD pipeline from scratch on the Oracle Cloud Infrastructure (OCI) and Microsoft Azure in less than 2 hours. 

The pipeline that we propose to build is the following:

![Pipeline.png](pics/Pipeline.png)

The application that we want to create is a python web application that gets its data from a MySQL database. Both the application and the database will be deployed as docker containers.

There are 3 environments in the CI/CD pipeline:
- a development environment (DEV) running on a vitual machine hosted on the Oracle Cloud 
- a test environment located on Azure where the application is dynamically deployed and tested after each commit
- a production environment on the Oracle Cloud that stays up and running all the time

We use GitLab as our code repository and CI/CD tool, and Docker Hub to store our docker images.
The execution of the CI/CD pipeline is controlled by a GitLab runners installed on the development virtual machine.

# Pre-requisites
### GitLab account
- If you do not have an account on GitLab already, create one
- Clone the https://gitlab.com/guillaume.goutaudier/devops-hybrid-cloud project into your own repository
- Register your ssh key and make sure you can use the git commands against your project
### Oracle Cloud account
- If you do not have an account on the Oracle Cloud already, you can request one on here:
https://www.oracle.com/cloud/free/
- The 30-day Free Trial account is sufficient for all the components that we use in this workshop
### Microsoft Azure account
- If you do not have an account on Microsoft Azure already, you can request one on here:
https://azure.microsoft.com/free/
- The Free Trial account is sufficient for all the components that we use in this workshop

# Infrastructure setup using Terraform   
We will install the following components automatically with Terraform:
- OKE Cluster (Oracle-managed Kubernetes)
- OCI network and instances

Install terraform on your laptop following this guide:
https://community.oracle.com/docs/DOC-1019936

If you need to generate the OCI API keys you can use the following script:
```
./scripts/generate_oci_api_key.sh
```

### OCI
Download the `terraform/` folder. Edit `oci/provider.tf` and update the `provider` section and `compartment_id` variable to match your OCI settings.

Then run the following command:
```
terraform init
terraform apply -target=null_resource.configure-instances
terraform apply -target=oci_containerengine_node_pool.pool
```

For conveniance, you can store the SSH key from the Terraform output to ssh-key:
```
vi ssh-key
chmod 600 ssh-key
```

Creating an alias like this one to connect to the VM can be useful:
```
alias so='ssh -i ~/devops-hybrid-cloud/terraform/oci/ssh-key -o ServerAliveInterval=30 ubuntu@<SERVER_IP>'
```

### Azure
Run the following command:
```
# If not already connected to azure, run 'az login'
terraform init
terraform apply
```

# OCI Dev VM setup
### Clone the workshop repository from GitLab:
First generate an SSH key and save it in GilLab (User profile / Settings / SSH Keys):
```
cat .ssh/id_rsa.pub 
# Copy/paste to GitLab
git clone git@gitlab.com:guillaume.goutaudier/devops-hybrid-cloud.git
```
### Install Python dependencies
```
pip3 install -r devops-hybrid-cloud/app/requirements.txt 
```
### Install docker
Execute the following script:
```
./devops-hybrid-cloud/scripts/install_docker_ubuntu.sh
```
### Install Kubectl
```
sudo snap install kubectl --classic
```
### Configure Kubectl for OCI
Install the Oracle Cloud Infrastructure CLI (instructions from https://docs.cloud.oracle.com/iaas/Content/ContEng/Tasks/contengdownloadkubeconfigfile.htm):
```
bash -c "$(curl -L https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh)"
oci setup config
cat ~/.oci/oci_api_key_public.pem
```
In the OCI console, go to your User Settings and add a new API key by copy/pasting the public key that we just generated.

From the OCI console go to **"Developer Services / Container Clusters (OKE)"**. Click on the cluster and then on "Access Kubeconfig". Generate the Kubectl config file by entering the commands that is displayed (step 2.).

Save the kubconfig file to :
```
mv ~/.kube/config ~/.kube/config-oci
```

Validate that you can connect to the cluster:
```
export KUBECONFIG=~/.kube/config-oci
kubectl get nodes
```

### Configure Kubectl for Azure
Save the `kube_config` terraform output to `~/.kube/config-azure`.
If you need to display the terraform outputs again, just run:
```
cd terraform/azure
terraform output
```

Validate that you can connect to the cluster:
```
export KUBECONFIG=~/.kube/config-azure
kubectl get nodes
```

### Install Gitlab Runner
Execute the following script:
```
./devops-hybrid-cloud/scripts/install_gitlab_runner.sh
```
### Register Gitlab Runner
In GitLab, go to "Settings / CI/CD / Runners" and write down your project registration token. Also make sure that "shared runners" are disabled.
```
sudo gitlab-runner register
```
When asked for the token, enter the one you previously noted. 


# Develop the application on the Dev environment

Create a Docker volume and start the MySQL DB container:
```
docker volume create dbvol
docker run --rm -it -d -p 33060:33060 --name mysqldb -e MYSQL_ROOT_PASSWORD=secret -v dbvol:/var/lib/mysql mysql:latest
```
Check the status of the container and that you can connect to the database:
```
docker ps
mysql -u root -p --password=secret -h 127.0.0.1 -e "SHOW VARIABLES LIKE 'version';"
```
Initialize the DB:
```
cd devops-hybrid-cloud/app
export MYSQL_ROOT_PASSWORD=secret
export DB_SERVICE_SERVICE_HOST=127.0.0.1
export DB_SERVICE_SERVICE_PORT=33060
export MYSQL_MESSAGE="Hello world from Dev"
python3 init_db.py
```
Test the application:
```
export MYSQL_HOST=localhost
export MYSQL_ROOT_PASSWORD=secret
python3 app.py
curl localhost:5000
curl localhost:5000/cities.json
curl localhost:5000/db
```

Now that the application works well, build a Docker image from it, and upload the image to DockerHub:
```
docker build -t guillaumegoutaudier/cloud-native:app ./app
docker login -u guillaumegoutaudier -p uve6ahN2aeG1pah0
docker push guillaumegoutaudier/cloud-native:app
```

# Deploy the application in the Prod environment
### Initialise the database
Note the subnet OCID from the `terraform output` command and update the `devops-hybrid-cloud/oke/db.yml` file with it. Then create the DB container and associated service:
```
cd devops-hybrid-cloud/oke
export KUBECONFIG=~/.kube/config-oci
kubectl create -f db.yml
```
Note the IP of the node on which the DB is running: 
```
kubectl describe service/db-service | grep "LoadBalancer Ingress:"
```
Initialize the database:
```
cd ../app
export MYSQL_ROOT_PASSWORD=secret
export DB_SERVICE_SERVICE_HOST=<IP noted in previous step>
export DB_SERVICE_SERVICE_PORT=33060
export MYSQL_MESSAGE='Hello world from production!'
python3 init_db.py
```

### Application micro-service on OKE
Deploy the application using the below command. This will create a deployment with 3 pods:
```
cd ../oke
kubectl create -f app_deployment.yml
```
Edit the `app_service.yml` file and replace the subnet OCID by the one that you created with Terraform (re-run `terraform output` if you need to display it again), then deploy the service:
```
kubectl create -f app_service.yml
```
Check in the OCI console that a Load Balancer has been automaticall created.
You can see the Load Balancer external IP using this command: 
```
kubectl describe service/app-service | grep "LoadBalancer Ingress:"
```

Test the application:
```
curl <IP_FROM_PREVIOUS_STEP>:5000
curl <IP_FROM_PREVIOUS_STEP>:5000/cities.json
curl <IP_FROM_PREVIOUS_STEP>:5000/db
```
# CI/CD pipeline
Now that everything is ready, we can run the CI/CD pipeline.

Make a modification to the application and commit your changes. This will trigger the execution of the pipeline. You can connect to GitLab to watch its execution. The steps of the pipeline are described in the `.gitlab-ci.yml` file. Below is an explanation of the main stages.

### Application Build
The application is built dynamically on the dev VM as a new docker image after each commit.
It is then uploaded on Docker Hub, from where it can be pushed to the QA and PROD environments.

### QA deployment
The QA environment is fully hosted on AKS.

2 micro-services are configured:
- the Database
- the Application
These micro-services are re-deployed at each build, and the database is re-initialized with a test dataset.

Some tests are then executed, and if all goes well the application is then updated in the PROD environment.

### PROD deployment
During the last stage of the CI/CD pipeline, the `deployment` is updated to use the latest build and automatically executes a rolling update (all pods are progressively replaced by new ones running the new version of the application). 

# Clean-up
Run the following command on oci-instance-1 and oci-instance-2:
```
export KUBECONFIG=~/.kube/config-oci
kubectl delete service,pod,deployment --all
export KUBECONFIG=~/.kube/config-azure
kubectl delete service,pod,deployment --all
```
Destroy all the OCI resource with Terraform:
```
cd terraform/azure
terraform destroy
cd terraform/oci
terraform destroy
```
Remove the API key from your User settings.

Remove the SSH keys and Runners from your Gitlab settings. 

