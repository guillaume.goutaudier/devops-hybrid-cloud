#!/bin/bash

kubectl get service/db-service > /dev/null 2>&1
if [ $? -eq 0 ]; then 
  echo "Deleting db service..."
  kubectl delete service/db-service
fi

kubectl get pod/db-pod > /dev/null 2>&1
if [ $? -eq 0 ]; then 
  echo "Deleting db pod..."
  kubectl delete pod/db-pod 
fi


