#!/bin/bash
echo -n "Waiting for oracledb container to start..."
while ! kubectl logs pod/db-pod | grep "The database is ready for use" ; 
do
       echo -n "."
       sleep 5
done   
echo "Database seems to be running OK. Giving it 1 more minute to finish initializing"
sleep 60

