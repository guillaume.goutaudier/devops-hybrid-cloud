import os, urllib.request

ip = os.environ.get('APP_IP')
app_path = os.environ.get('APP_PATH',"")
url = 'http://'+ip+':5000'+app_path

print("Connecting to "+url)
response = urllib.request.urlopen(url)
data = response.read()

print("URL response:", data.decode('utf-8'))

