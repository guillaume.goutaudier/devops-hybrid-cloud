#!/bin/bash
echo -n "Waiting for application load balancer IP to be assigned..."
while kubectl get service/app-service | grep pending ; 
do
       echo -n "."
       sleep 5
done   
echo "App load balancer ready"
