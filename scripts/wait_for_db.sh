#!/bin/bash
echo -n "Waiting for application container to start..."
while ! kubectl describe pod/db-pod | grep Status: | grep Running ; 
do
       echo -n "."
       sleep 1
done   
echo "DB pod is now ready"
echo "Giving MySQL 60 more seconds to be ready to process requests"
sleep 60

