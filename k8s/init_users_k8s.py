import os, cx_Oracle
from get_service_ip import get_service_ip

cluster_ip = get_service_ip('db-service')
connection_str = cluster_ip+":1521/ORCLCDB.localdomain"
print("Connecting to "+connection_str)
connection = cx_Oracle.connect("SYS", "Oradoc_db1", connection_str, mode=cx_Oracle.SYSDBA)
cursor = connection.cursor()
cursor.execute("alter session set \"_ORACLE_SCRIPT\"=true")
cursor.execute("create user guillaume identified by boong0EiTah9Onoh##")
cursor.execute("GRANT CONNECT, RESOURCE, DBA TO guillaume")
cursor.execute("COMMIT")
print("Successfully added user guillaume to the database")
cursor.close()
connection.close()

