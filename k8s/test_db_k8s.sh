#!/bin/bash
IP=$(python3 ./k8s/get_service_ip.py 2>/dev/null)
echo "Waiting for mysql container to accept connections:"
until $(mysql -u root -p --password=secret --connect-timeout=1 -h $IP -e "show databases" >/dev/null 2>&1) ; do
	echo -n "."
	IP=$(python3 ./k8s/get_service_ip.py 2>/dev/null)
	sleep 1
done
echo "Mysql container now ready!"
