resource "tls_private_key" "ssh_key" {
  algorithm   = "RSA"
}

# ------ Create two VM.Standard2.1 instances
resource "oci_core_instance" "instances" {
  count = 2
  availability_domain = "${lookup(data.oci_identity_availability_domains.ads.availability_domains[0],"name")}"
  compartment_id = "${var.compartment_id}"
  display_name = "${var.prefix}-instance${count.index+1}"
  hostname_label = "${var.prefix}-instance${count.index+1}"
  image = "${lookup(data.oci_core_images.ubuntu-images.images[0], "id")}"
  shape = "VM.Standard2.1"
  subnet_id = "${oci_core_subnet.public-subnet1.id}"
  metadata = {
    ssh_authorized_keys = "${tls_private_key.ssh_key.public_key_openssh}"
  }
  timeouts {
    create = "30m"
  }
}
resource "null_resource" "configure-instances" {
  count = 2
  triggers = {
    master_ip = "${element(oci_core_instance.instances.*.public_ip, count.index)}"
  }
  provisioner "remote-exec" {
    connection {
      agent       = false
      timeout     = "30m"
      host        = "${element(oci_core_instance.instances.*.public_ip, count.index)}"
      user        = "ubuntu"
      private_key = "${tls_private_key.ssh_key.private_key_pem}"
    }
    inline = [
      "rm -rf  /home/ubuntu/.ssh/id_rsa",
      "ssh-keygen -f /home/ubuntu/.ssh/id_rsa -N ''",
      "until $( test -f /var/lib/cloud/instance/boot-finished ); do sleep 1; done",
      "sudo apt-get -qq update",
      "sudo apt-get -qq install mysql-client-core-5.7",
      "sudo apt-get -qq install python3-pip"
    ]
  }
}

