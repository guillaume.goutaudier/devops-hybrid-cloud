provider "azurerm" {
}

resource "azurerm_kubernetes_cluster" "test" {
  name                = "acctestaks1"
  location            = "francecentral"
  resource_group_name = "free_resource_group"
  dns_prefix          = "acctestagent1"

  agent_pool_profile {
    name            = "pool1"
    count           = 1
    vm_size         = "Standard_B2s"
    os_type         = "Linux"
    os_disk_size_gb = 30
  }

  service_principal {
    client_id     = "019beb86-0278-4c97-af9f-da7e1f2109cc"
    client_secret = "7da55ed9-b3e3-4fb2-9801-7fc1ee004c3b"
  }

  tags = {
    Environment = "Test"
  }
}

output "kube_config" {
  value = "${azurerm_kubernetes_cluster.test.kube_config_raw}"
}


