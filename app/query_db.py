# Used for troubleshooting purposes only
import os, mysqlx

def query_db():
  mysql_password = os.environ.get('MYSQL_ROOT_PASSWORD')
  mysql_host = os.environ.get('DB_SERVICE_SERVICE_HOST')
  mysql_port = os.environ.get('DB_SERVICE_SERVICE_PORT')
  mysql_message = os.environ.get('MYSQL_MESSAGE')
  session = mysqlx.get_session(user='root', password=mysql_password, host=mysql_host, port=mysql_port)
  schema = session.get_schema("schema")
  collection = schema.get_collection("collection")
  result = collection.find("type = 'message'").execute()
  doc = result.fetch_one()
  session.close()
  return doc.value

if __name__ == '__main__':
  print(query_db())


