import os, mysqlx
mysql_password = os.environ.get('MYSQL_ROOT_PASSWORD')
mysql_host = os.environ.get('DB_SERVICE_SERVICE_HOST')
mysql_port = os.environ.get('DB_SERVICE_SERVICE_PORT')
mysql_message = os.environ.get('MYSQL_MESSAGE')
session = mysqlx.get_session(user='root', password=mysql_password, host=mysql_host, port=mysql_port)
schema = session.create_schema("schema")
collection = schema.create_collection("collection")
collection.add({"type": "message", "value": mysql_message}).execute()
session.close()

