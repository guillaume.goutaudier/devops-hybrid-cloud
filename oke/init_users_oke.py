import os, cx_Oracle
from get_service_external_ip import get_service_external_ip

cluster_ip = get_service_external_ip('db-service')
connection = cx_Oracle.connect("SYS", "Oradoc_db1", cluster_ip+":1521/ORCLCDB.localdomain", mode=cx_Oracle.SYSDBA)
cursor = connection.cursor()
cursor.execute("alter session set \"_ORACLE_SCRIPT\"=true")
cursor.execute("create user guillaume identified by boong0EiTah9Onoh##")
cursor.execute("GRANT CONNECT, RESOURCE, DBA TO guillaume")
cursor.execute("COMMIT")
cursor.close()
connection.close()

